#!/usr/bin/env bash
echo "What hostname should I use?"
read NEWHOSTNAME
# NEWHOSTNAME="HOSTNAME"


sudo sed -i.bak "s/raspberrypi/$NEWHOSTNAME/g" /etc/hosts /etc/hostname
sudo hostname $NEWHOSTNAME
# Check if this line exist
grep -q gpu_mem=16 /boot/config.txt || sudo sh -c "echo "gpu_mem=16" >> /boot/config.txt"
echo "############################################################"
echo "Updating system"
echo "############################################################"
sudo apt update && sudo apt -y full-upgrade
echo "############################################################"
echo "Installing docker"
echo "############################################################"
if [ ! -d /etc/docker ]; then
  curl -sSL https://get.docker.com | sh
fi
echo "Enabling docker"
sudo systemctl enable docker
echo "Starting docker"
sudo systemctl start docker
echo "Adding user to docker"
sudo usermod -aG docker $USER
newgrp docker << BLEBLE
newgrp $USER
BLEBLE
echo "Installing docker-compose"
sudo apt-get install docker-compose -y
# sudo apt-get -y install python-setuptools && sudo easy_install pip  && sudo pip install docker-compose
echo "Enabling docker-compose at boot"
sudo cp $HOME/rpi.docker-compose/docker-compose-opt.service /etc/systemd/system/docker-compose-opt.service

echo "Replacing user with '$USER'" in docker-compose systemd
user=$(whoami)
sudo sed -i "s/<user>/$user/" /etc/systemd/system/docker-compose-opt.service

echo "Replacing path to docker in docker-compose systemcd"
pathtocompose=$(which docker-compose)
echo $pathtocompose
sudo sed -i "s|<path>|$pathtocompose|" /etc/systemd/system/docker-compose-opt.service


sudo systemctl enable docker-compose-opt
cp $HOME/rpi.docker-compose/docker-compose.yaml $HOME/docker-compose.yaml
while true; do
    read -p "Do you wish to clone docker-compose examples? [y/n]" yn
    case $yn in
        [Yy]* ) git clone https://gitlab.com/pblgomez/rpi.dockers.git $HOME/rpi.dockers ; break;;
        [Nn]* ) exit;;
        * ) echo "Please answer y or n.";;
    esac
done
